using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Core.Repositories;

namespace WebApplication.Mobile.Features.News
{
    public class CreateNewsCommand : IRequest
    {
        public long Id { get; set; }
        public string ImagedId { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        [FromQuery]
        public string SomeVar { get; set; }
    }
    
    public class GetListNewsQueryHandler : AsyncRequestHandler<CreateNewsCommand>
    {
        private readonly INewsRepository _newsRepository;

        public GetListNewsQueryHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        protected override async Task Handle(CreateNewsCommand request, CancellationToken cancellationToken)
        {
            var news = new Core.Model.News()
            {
                Id = request.Id,
                Image = request.ImagedId,
                Text = request.Text,
                Title = request.Title
            };

            await _newsRepository.Add(news, cancellationToken);
        }
    }
}