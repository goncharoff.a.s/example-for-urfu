﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Mobile.Features.News;

namespace WebApplication.Mobile.Controllers
{
    public class NewsController : BackofficeBaseController
    {
        private readonly IMediator _mediator;

        public NewsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("/api/news/createNews")]
        public async Task<IActionResult> GetNewsList([FromBody]CreateNewsCommand command)
        {
            await _mediator.Send(command, this.HttpContext.RequestAborted);
            return Ok();
        }
        
    }
}