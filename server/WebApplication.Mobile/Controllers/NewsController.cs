﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Mobile.Features.News;

namespace WebApplication.Mobile.Controllers
{
    public class NewsController : MobileBaseController
    {
        private readonly IMediator _mediator;

        public NewsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("/api/news/getNewsList")]
        public async Task<IActionResult> GetNewsList([FromQuery]GetListNewsQuery query)
        {
            var news = await _mediator.Send(query, this.HttpContext.RequestAborted);
            return Ok(news);
        }
        
        [HttpGet("/api/news/getNews")]
        public async Task<IActionResult> GetNewsList([FromQuery]GetNewsQuery query)
        {
            var news = await _mediator.Send(query, this.HttpContext.RequestAborted);
            return Ok(news);
        }
    }
}