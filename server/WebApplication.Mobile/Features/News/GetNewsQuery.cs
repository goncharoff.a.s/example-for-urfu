using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WebApplication.Core.Repositories;
using WebApplication.Mobile.Model;

namespace WebApplication.Mobile.Features.News
{
    public class GetNewsQuery : IRequest<NewsDto>
    {
        public long Id { get; set; }
    }
    
    public class GetNewsQueryHandler : IRequestHandler<GetNewsQuery, NewsDto>
    {
        private readonly INewsRepository _newsRepository;

        public GetNewsQueryHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }
        
        public async Task<NewsDto> Handle(GetNewsQuery request, CancellationToken cancellationToken)
        {
            var news = await _newsRepository.GetNews(request.Id, cancellationToken);
            return new NewsDto()
            {
                Id = news.Id,
                Image = news.Image,
                Text = news.Text,
                Title = news.Title
            };
        }
    }
}