using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using WebApplication.Core.Repositories;
using WebApplication.Mobile.Model;

namespace WebApplication.Mobile.Features.News
{
    public class GetListNewsQuery : IRequest<ListNewsDto>
    {
        public int? Count { get; set; }
        public int? Skip { get; set; }
    }
    
    public class GetListNewsQueryHandler : IRequestHandler<GetListNewsQuery, ListNewsDto>
    {
        private readonly INewsRepository _newsRepository;

        public GetListNewsQueryHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }
        
        public async Task<ListNewsDto> Handle(GetListNewsQuery request, CancellationToken cancellationToken)
        {
            var news = await _newsRepository.GetNewsList(request.Skip, request.Count, cancellationToken);
            
            //IEnumerable<T> vs IReadOnlyCollection<T> 
            
            var listNewsDto = new ListNewsDto();
            listNewsDto.Items = news.Select(x => new ListNewsDto.ListItemNewsDto()
            {
                Id = x.Id,
                Title = x.Title,
                ShortName = x.Text.Substring(10)
            }).ToArray();
            
            return listNewsDto;
        }
    }
}