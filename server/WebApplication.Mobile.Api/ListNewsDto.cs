namespace WebApplication.Mobile.Model
{
    public class ListNewsDto
    {
        public ListItemNewsDto[] Items { get; set; } = new ListItemNewsDto[0];
        public class ListItemNewsDto
        {
            public long Id { get; set; }
            public string Title { get; set; }
            public string ShortName { get; set; }
        }
    }
}