using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication.Core.Model;
using WebApplication.Core.Repositories;

namespace WebApllication.Infrastructure.Repository
{
    public class InMemoryNewsRepository : INewsRepository
    {
        private static List<News> _data = new List<News>
        {
            new News()
            {
                Id = 1,
                Image = "sdfdsfdsfsdfsdf",
                Text = "Супер новость 1",
                Title = "Невероятный заголовок 1"
            },
            
            new News()
            {
                Id = 2,
                Image = "sdfdsfdsfsdfsdf 2",
                Text = "Супер новость 2",
                Title = "Невероятный заголовок 2"
            }
            ,
            new News()
            {
                Id = 3,
                Image = "sdfdsfdsfsdfsdf 3",
                Text = "Супер новость 3",
                Title = "Невероятный заголовок 4"
            }
        };
        
        public Task<IReadOnlyCollection<News>> GetNewsList(int? skip, int? count, CancellationToken cancellationToken)
        {
            var news = _data.AsEnumerable();
            if (skip.HasValue && count.HasValue)
            {
                news = news.Skip(skip.Value).Take(count.Value);
            }

            var result = new ReadOnlyCollection<News>(news.ToList());
            return Task.FromResult((IReadOnlyCollection<News>)result);
        }

        public Task<News> GetNews(long id, CancellationToken cancellationToken)
        {
            return Task.FromResult<News>(_data.FirstOrDefault(x => x.Id == id));
        }

        public Task Add(News news, CancellationToken cancellationToken)
        {
            _data.Add(news);
            return Task.CompletedTask;
        }
    }
}