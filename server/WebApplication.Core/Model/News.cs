namespace WebApplication.Core.Model
{
    public class News
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
    }
}