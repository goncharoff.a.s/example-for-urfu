using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using WebApplication.Core.Model;

namespace WebApplication.Core.Repositories
{
    public interface INewsRepository
    {
        Task<IReadOnlyCollection<News>> GetNewsList(int? skip, int? count, CancellationToken cancellationToken);
        Task<News> GetNews(long id, CancellationToken cancellationToken);
        Task Add(News news, CancellationToken cancellationToken);
    }
}