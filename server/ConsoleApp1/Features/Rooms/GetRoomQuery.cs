using System;
using WebApplication.Core;
using WebApplication.Core.Repositories;

namespace ConsoleApp1.Features.Rooms
{
    public class GetRoomQuery
    {
        public long Id { get; set; }
    }

    public class GetRoomQueryHandler
    {
        private readonly RoomService _roomService;
        public GetRoomQueryHandler(RoomService roomService)
        {
            _roomService = roomService;
        }

        public Room Handle(GetRoomQuery query)
        {
            return _roomService.GetRoom(query.Id);
        }
    }
    
    public class GetRoomQueryLogHandler
    {
        private readonly GetRoomQueryHandler _handler;

        public GetRoomQueryLogHandler(GetRoomQueryHandler handler)
        {
            _handler = handler;
        }

        public Room Handle(GetRoomQuery query)
        {
            Console.WriteLine("Start get room");
            var room = _handler.Handle(query);
            Console.WriteLine("Stop get room");
            return room;
        }
    }
}