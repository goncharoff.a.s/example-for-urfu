﻿using System;
using ConsoleApp1.Features.Rooms;
using WebApllication.Infrastructure;
using WebApplication.Core;
using WebApplication.Core.Repositories;

namespace ConsoleApp1
{
    class Program
    {
        public static IRoomRepository _roomRepository = new RoomRepository();
        
        static void Main(string[] args)
        {
            var query = new GetRoomQuery()
            {
                //Id = 1
            };
            var scopedRoomRepository = new RoomRepository();

            var handler = new GetRoomQueryLogHandler(new GetRoomQueryHandler(new RoomService(_roomRepository)));
            
            var room = handler.Handle(query);

            scopedRoomRepository.LoadRooms();

            for (int i = 0; i < 10; i++)
            {
                var transientRoomRepository = new RoomRepository();
            }
            
            Console.WriteLine(room);

            Console.ReadKey();
        }
    }
}