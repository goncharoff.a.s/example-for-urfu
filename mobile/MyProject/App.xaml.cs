﻿using System;
using MyProject.Services;
using MyProject.Services.Fakes;
using MyProject.Services.Implementations;
using MyProject.Views;
using MyProject.Views.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyProject
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //Регистрация сервисов
            RegisterServices(false);

            MainPage = new NavigationPage(new NewsListPage());
        }

        private async void RegisterServices(bool enableFakes)
        {
            Service<INotificationService>.Register(new NotificationService());

            if (enableFakes)
            {
                Service<ILoginService>.Register(new FakeLoginService());
                Service<INewsService>.Register(new FakeNewsService());
            }
            else
            {
                Service<ILoginService>.Register(new LoginService());
                Service<INewsService>.Register(new NewsService());
                Service<IConnectionService>.Register(new ConnectionService());
            }

            //await SecureStorage.SetAsync("sid", Guid.NewGuid().ToString());

            //var test = await SecureStorage.GetAsync("sid");// "2d5afb56-17fe-4e16-88a4-08b941b5484b"

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
