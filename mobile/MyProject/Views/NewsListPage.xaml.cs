﻿using System;
using System.Collections.Generic;
using MyProject.DTOs;
using MyProject.ViewModels;
using Xamarin.Forms;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.Views
{
    public partial class NewsListPage : ContentPage, INewsListPage
    {
        public NewsListPage()
        {
            InitializeComponent();

            var viewModel = new NewsListPageVM(this);
            this.BindingContext = viewModel;
        }

        public async void ShowNewsItemPage(ListItemNewsDto dto)
        {
            await Navigation.PushAsync(new NewsPage(dto));
        }
    }
}