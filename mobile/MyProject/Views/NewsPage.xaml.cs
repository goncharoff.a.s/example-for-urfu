﻿using System;
using System.Collections.Generic;
using MyProject.DTOs;
using MyProject.ViewModels.NewsPage;
using Xamarin.Forms;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.Views
{
    public partial class NewsPage : ContentPage, INewsPage
    {
        public NewsPage(ListItemNewsDto dto)
        {
            InitializeComponent();

            this.BindingContext = new NewsPageVM(dto, this);
        }
    }
}
