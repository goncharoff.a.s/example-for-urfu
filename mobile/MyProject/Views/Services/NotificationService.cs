﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MyProject.Services;
using Xamarin.Forms;

namespace MyProject.Views.Services
{
    public class NotificationService : INotificationService
    {
        public Task ShowNotification(string message)
        {
            var page = Application.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            return page?.DisplayAlert("Ошибка", message, "Ок");
        }
    }
}
