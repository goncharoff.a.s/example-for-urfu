﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyProject.DTOs;
using MyProject.ViewModels;
using MyProject.Views;
using Xamarin.Forms;

namespace MyProject
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            var mainPageVM = new LoginPageVM(ShowNextPage);

            this.BindingContext = mainPageVM;
        }

        public async void ShowNextPage(UserDTO user)
        {
            //var listPageVM = new ListPageVM(user);
            //var listpage = new NewsListPage(listPageVM);
            //await Navigation.PushAsync(listpage);
        }
    }
}
