﻿using System;
using System.Threading.Tasks;

namespace MyProject.Services
{
    public interface INotificationService
    {
        Task ShowNotification(string message);
    }
}
