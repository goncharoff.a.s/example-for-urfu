﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyProject.DTOs;
using WebApplication.Mobile.Model;

namespace MyProject.Services
{
    public interface INewsService
    {
        Task<ListNewsDto> LoadListNews(); 
        Task<NewsDto> LoadNews(long id);
        Task CreateNews(NewsDto news);
    }
}
