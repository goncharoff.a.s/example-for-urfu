﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MyProject.Services.Implementations
{
    public class ConnectionService : IConnectionService
    {
        private HttpClient _httpClient;
        public ConnectionService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> GetRequest(string url)
        {
            var response = await _httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

        public async Task<string> PostRequest(string url, StringContent content)
        {
            var response = await _httpClient.PostAsync(url, content);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}