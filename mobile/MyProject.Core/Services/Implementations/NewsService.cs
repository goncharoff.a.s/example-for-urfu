﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MyProject.DTOs;
using Newtonsoft.Json;
using WebApplication.Mobile.Model;

namespace MyProject.Services.Implementations
{
    public class NewsService : INewsService
    {
        public async Task<ListNewsDto> LoadListNews()
        {
            var result = await Context.Connection.GetRequest("http://192.168.1.56:5000/api/news/getNewsList");
            var dto = JsonConvert.DeserializeObject<ListNewsDto>(result);
            return dto;
        }

        public async Task<NewsDto> LoadNews(long id)
        {
            var result = await Context.Connection.GetRequest($"http://192.168.1.56:5000/api/news/getNews?id={id}");
            var dto = JsonConvert.DeserializeObject<NewsDto>(result);
            return dto;
        }

        public async Task CreateNews(NewsDto news)
        {
            string json = JsonConvert.SerializeObject(news);
            var content = new StringContent(json);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");
            var result = await Context.Connection.PostRequest("http://192.168.1.56:5000/api/news/createNews", content);
        }
    }
}
