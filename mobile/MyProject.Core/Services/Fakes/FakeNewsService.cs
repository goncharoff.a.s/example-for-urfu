﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyProject.DTOs;
using WebApplication.Mobile.Model;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.Services.Fakes
{
    public class FakeNewsService : INewsService
    {
        private List<ListItemNewsDto> _news = new List<ListItemNewsDto>();
        public FakeNewsService()
        {
            _news.Add(new ListItemNewsDto
            {
                Id = 1,
                Title = "Новость 1",
                ShortName = @"Флагманский чип, выполненный по 5-нанометровому техпроцессу, в числе первых получат смартфоны брендов Xiaomi, Redmi, Black Shark, Vivo, iQOO, OPPO, Realme и OnePlus. Интересно, что в гонке даже наметился лидер — первым на рынке смартфоном на новой платформе должен стать Xiaomi Mi 11, презентация которого состоится до конца 2020 года."
            });
            _news.Add(new ListItemNewsDto
            {
                Id = 2,
                Title = "Новость 2",
                ShortName = @" Блокировка и замедление трафика интернет-платформ за цензуру российских СМИ могут применяться вместе со штрафами, так как, а блокировка отдельных страниц или ресурса полностью будет зависеть от возможностей самой интернет-платформы, заявил РИА Новости один из авторов законопроекта, глава комитета Госдумы по информполитике Александр Хинштейн."
            });
        }

        public Task CreateNews(NewsDto news)
        {
            throw new NotImplementedException();
        }

        public async Task<ListNewsDto> LoadListNews()
        {
            await Task.Delay(2000);

            return new ListNewsDto()
            {
                Items = _news.Select(x=> new ListItemNewsDto
                {
                    Title =  x.Title,
                    ShortName = x.ShortName.Substring(0,20)
                }).ToArray()
            };
        }

        public async Task<NewsDto> LoadNews(long id)
        {
            await Task.Delay(2000);
            var news = _news.FirstOrDefault(x => x.Id == id);

            if (news == null)
                throw new Exception("Такой новости не существует");

            return new NewsDto
            {
                Id = id,
                Image = "",
                Text = news.ShortName,
                Title = news.Title
            };
        }
    }
}
