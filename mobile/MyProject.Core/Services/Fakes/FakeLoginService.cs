﻿using System;
using System.Threading.Tasks;
using MyProject.DTOs;

namespace MyProject.Services.Fakes
{
    public class FakeLoginService : ILoginService
    {

        public async Task<UserDTO> Authorise(string login, string password)
        {
            await Task.Delay(2000);

            if (login != "123")
                throw new Exception("Пользователь не найден!");


            if (password != "321")
                throw new Exception("Неправильный пароль!");

            return new UserDTO()
            {
                Id = "1",
                Name = "Вася",
                LastName = "Пупкин"
            };
        }
    }
}