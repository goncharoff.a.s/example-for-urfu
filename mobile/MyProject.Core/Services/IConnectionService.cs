﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyProject.Services
{
    public interface IConnectionService
    {
        Task<string> GetRequest(string url);
        Task<string> PostRequest(string url, StringContent content);
    }
}
