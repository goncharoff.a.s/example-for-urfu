﻿using System;
using System.Net;
using System.Threading.Tasks;
using MyProject.DTOs;

namespace MyProject.Services
{
    public interface ILoginService
    {
        Task<UserDTO> Authorise(string login, string password);
    }
}
