﻿using System;
using MyProject.Services;

namespace System
{
    public static class Context
    {
        public static INewsService NewsService => Service<INewsService>.GetInstance();
        public static ILoginService LoginService => Service<ILoginService>.GetInstance();
        public static INotificationService NotificationService => Service<INotificationService>.GetInstance();
        public static IConnectionService Connection => Service<IConnectionService>.GetInstance();
    }
}
