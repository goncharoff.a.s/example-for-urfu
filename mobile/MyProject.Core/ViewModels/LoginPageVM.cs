﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using MyProject.DTOs;
using MyProject.Services;

namespace MyProject.ViewModels
{
    public class LoginPageVM : BasePageVM
    {
        private string _loginText;
        public string LoginText
        {
            get
            {
                return _loginText;
            }
            set
            {
                if (_loginText != value)
                {
                    _loginText = value;
                    InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(LoginText)));
                    
                }
            }
        }

        private string _passwordText;
        public string PasswordText
        {
            get
            {
                return _passwordText;
            }
            set
            {
                if (_passwordText != value)
                {
                    _passwordText = value;
                    InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(PasswordText)));
                }
            }
        }

        public ICommand LoginButton { get; set; }

        private Action<UserDTO> _showNextPageAction;

        public LoginPageVM(Action<UserDTO> showNextPageAction)
        {
            _showNextPageAction = showNextPageAction;
            LoginButton = new Command(Authorise);
        }

        private async void Authorise()
        {
            try
            {
                IsLoading = true;
                var user = await Context.LoginService.Authorise(_loginText, _passwordText);
                _showNextPageAction?.Invoke(user);
            }
            catch (Exception ex)
            {
                await Context.NotificationService.ShowNotification(ex.Message);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}