﻿using System;
using System.ComponentModel;
using MyProject.DTOs;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.ViewModels.NewsPage
{
    public class NewsPageVM : BasePageVM
    {
        private ListItemNewsDto _dto;
        private INewsPage _page;

        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(Text)));
                }
            }
        }

        public NewsPageVM(ListItemNewsDto dto, INewsPage page)
        {
            _dto = dto;
            _page = page;

            LoadData();
        }

        private async void LoadData()
        {
            this.IsLoading = true;

            try
            {
                var news = await Context.NewsService.LoadNews(_dto.Id);
                Text = news.Text;
            }
            catch (Exception ex)
            {
                await Context.NotificationService.ShowNotification(ex.Message);
            }
            finally
            {
                this.IsLoading = false;
            }
        }
    }
}
