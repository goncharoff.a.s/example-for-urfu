﻿using System;
using MyProject.DTOs;
using WebApplication.Mobile.Model;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.ViewModels
{
    public class NewsListIemVM
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public ListItemNewsDto DTO { get; private set; }

        public NewsListIemVM(ListItemNewsDto dto)
        {
            Title = dto.Title;
            Text = dto.ShortName;
            DTO = dto;
        }
    }
}
