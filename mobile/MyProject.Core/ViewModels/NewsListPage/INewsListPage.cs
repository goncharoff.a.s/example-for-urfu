﻿using System;
using MyProject.DTOs;
using static WebApplication.Mobile.Model.ListNewsDto;

namespace MyProject.ViewModels
{
    public interface INewsListPage
    {
        void ShowNewsItemPage(ListItemNewsDto dto);
    }
}
