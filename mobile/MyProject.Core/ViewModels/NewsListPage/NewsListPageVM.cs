﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using MyProject.DTOs;
using MyProject.Services;
using WebApplication.Mobile.Model;

namespace MyProject.ViewModels
{
    public class NewsListPageVM : BasePageVM
    {

        public ICommand CreateNewsButton { get; set; }

        private ObservableCollection<NewsListIemVM> _items; 
        public ObservableCollection<NewsListIemVM> Items
        {
            get
            {
                return _items;
            }
            set
            {
                if (_items != value)
                {
                    _items = value;
                    InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(Items)));

                }
            }
        }

        private NewsListIemVM _selectedItem;
        public NewsListIemVM SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
                }
            }
        }

        INewsListPage _page;


        public NewsListPageVM(INewsListPage page)
        {
            _page = page;

            Items = new ObservableCollection<NewsListIemVM>();

            LoadData();

            this.PropertyChanged += NewsListPageVM_PropertyChanged;

            CreateNewsButton = new Command(CreateTestNews);
        }

        int _count = 3;
        private async void CreateTestNews()
        {
            var news = new NewsDto()
            {
                Id = _count++,
                Text = $"Текст новости {_count}",
                Title = $"Заголовок новости {_count}"
            };

            await Context.NewsService.CreateNews(news);
            LoadData();
        }

        private void NewsListPageVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedItem))
            {
                if (SelectedItem == null)
                    return;

                _page.ShowNewsItemPage(SelectedItem.DTO);

                SelectedItem = null;
            }
        }

        private async void LoadData()
        {
            this.IsLoading = true;

            try
            {
                var news = await Context.NewsService.LoadListNews();
                Items = new ObservableCollection<NewsListIemVM>(news.Items.Select(x=> new NewsListIemVM(x)).ToList());
            }
            catch (Exception ex)
            {
                await Context.NotificationService.ShowNotification(ex.Message);
            }
            finally
            {
                this.IsLoading = false;
            }
        }
    }
}